// DEPENDECIES
const express = require('express');

// SERVER & MIDDLEWARE
const port = 5000;
const app = express();
app.use(express.json());

const newUser = {
    firstName: 'John',
    lastName: 'Dela Cruz',
    age: 18,
    conctactNo: '09123456789',
    batchNumber: 151,
    email: 'john.delacruz@gmail.com',
    password: 'SixTeenCharacter'
}

module.exports = {
    newUser: newUser
}

// SERVER STARTUP
app.listen(port, () => {
    console.log(`Server is running on ${port}!`)
});