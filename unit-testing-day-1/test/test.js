const { assert } = require('chai');
const { newUser } = require('../index.js')

describe('Test newUser object', () => {
    it('Assert newUser type is object', () => {
        assert.equal(typeof newUser, 'object');
    })
    // FIRST NAME
    it('Assert First Name type is string', () => {
        assert.equal(typeof newUser.firstName, 'string');
    })
    it('Assert First Name Name type is not undefined', () => {
        assert.notEqual(typeof newUser.firstName, 'undefined');
    })

    // LAST NAME
    it('Assert Last Name type is string', () => {
        assert.equal(typeof newUser.lastName, 'string');
    })
    it('Assert Last Name type is not undefined', () => {
        assert.notEqual(typeof newUser.lastName, 'undefined');
    })

    // AGE
    it('Assert Age is at least 18', () => {
        assert.isAtLeast(newUser.age, 18);
    })
    it('Assert Age type is number', () => {
        assert.equal(typeof newUser.age, 'number');
    })

    // CONTACT
    it('Assert Contact type is string', () => {
        assert.equal(typeof newUser.conctactNo, 'string');
    })

    // BATCH NUMBER
    it('Assert Batch type is number', () => {
        assert.equal(typeof newUser.batchNumber, 'number');
    })
    it('Assert Batch Number type is not undefined', () => {
        assert.notEqual(typeof newUser.batchNumber, 'undefined');
    })

    // EMAIL
    it('Assert email type is string', () => {
        assert.equal(typeof newUser.email, 'string');
    })
    it('Assert email type is not undefined', () => {
        assert.notEqual(typeof newUser.email, 'undefined');
    })

    // PASSWORD
    it('Assert password type is string', () => {
        assert.equal(typeof newUser.password, 'string');
    })
    it('Assert password length is 16 characters', () => {
        assert.equal(newUser.password.length, 16);
    })
})